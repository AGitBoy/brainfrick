/*
 * Copyright (C) 2019 Aidan Williams
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <stdint.h>

// Number of cells
#define MEM_SIZE 65535

// Use 8-bit cells
typedef uint8_t cell_t;

char *progbuff;

void readf(char *fname)
{
    FILE *f = fopen(fname, "rb");

    if (!f) {
        fprintf(stderr, "brainfrick: %s: %s\n", fname, strerror(errno));
        exit(errno);	
    }

    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);

    progbuff = malloc(fsize + 1);
    fread(progbuff, 1, fsize, f);

    fclose(f);

    progbuff[fsize] = 0;
}

void interpret()
{
    unsigned int pointer = 0;
    unsigned int position = 0;
    cell_t mem[MEM_SIZE];

    int offset, i;

    while (position < strlen(progbuff) - 1) {
        switch (progbuff[position]) {
        case '>': pointer++; position++; break;
        case '<': pointer--; position++; break;
        case '+': mem[pointer]++; position++; break;
        case '-': mem[pointer]--; position++; break;
        case '.': putchar(mem[pointer]); position++; break;
        case ',': mem[pointer] = getchar(); position++; break;
        case '[':
            if (mem[pointer] == 0) {
                offset = 0;
                i = position + 1;

                while (offset != -1) {
                    if (progbuff[i] == '\0') {
                        fprintf(stderr,
                                "brainfrick: unmatched '[' at char %i\n",
                                position);

                        exit(1);
                    }

                    if (progbuff[i] == '[') {
                        offset += 1;
                    } else if (progbuff[i] == ']' && offset > 0) {
                        offset -= 1;
                    } else if (progbuff[i] == ']' && offset == 0) {
                        position = i + 1;
                        break;
                    }

                    i++;
                }
            } else {
                position++;
            }

            break;
        case ']':
            if (mem[pointer] != 0) {
                offset = 0;
                i = position - 1;

                while (offset != -1) {
                    if (i == -1) {
                        fprintf(stderr,
                                "brainfrick: unmatched ']' at char %i\n",
                                position);

                        exit(1);
                    }

                    if (progbuff[i] == ']') {
                        offset += 1;
                    } else if (progbuff[i] == '[' && offset > 0) {
                        offset -= 1;
                    } else if (progbuff[i] == '[' && offset == 0) {
                        position = i;
                        break;
                    }

                    i--;
                }
            } else {
                position++;
            }

            break;
        default:
            position++;
            break;
        }
    }
}

int main(int argc, char **argv)
{
    if (argc <= 1) {
        fprintf(stderr, "Usage: brainfrick [FILE]\n");
        return 1;
    }

    readf(argv[1]);
    interpret();
    free(progbuff);
    return 0;
}
