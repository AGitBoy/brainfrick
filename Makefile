.POSIX:
.SUFFIXES:
CC     = cc
BIN    = brainfrick
SRC    = brainfrick.c
PREFIX = /usr/local
BINP   = 755

OBJ = $(SRC:.c=.o)

all: $(BIN)

$(BIN): $(OBJ)
	$(CC) $(CFLAGS) -o $@ $(OBJ)

.SUFFIXES: .c .o
.c.o:
	$(CC) $(CFLAGS) -c $<

.PHONY: clean install
clean:
	rm -f $(BIN) $(OBJ)

install: all
	strip $(BIN)
	install -d $(DESTDIR)$(PREFIX)/bin
	install -m $(BINP) $(BIN) $(DESTDIR)$(PREFIX)/bin
