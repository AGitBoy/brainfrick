# Brainfrick
A simple C implementation of brainfuck, the program is complete enough to run the game of life
implementation, albeit slowly.

## Language
The language is rather simple, with 8 instructions

| Command | Description                                                      |
|---------|------------------------------------------------------------------|
| >       | Move pointer to right                                            |
| <       | Move pointer to left                                             |
| +       | Increment memory under pointer                                   |
| -       | Decrement memory under pointer                                   |
| .       | Output character held in memory under pointer                    |
| ,       | Input character from stdin to memory under pointer               |
| [       | Jump past the matching ] if the memory under pointer is 0        |
| ]       | Jump back to the matching [ if the memory under pointer is not 0 |

## Notes
* There are still some bugs with the language, so it can't run all 
  brainfuck applications, but it should be able to do most.
* EOF is not handled by the interpreter, so it should be -1
